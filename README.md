# Mongo docker

Mini projet pour faire tourner une base de données Mongo dans un container local et pouvoir analyser les logs mongo. Il permet également de lancer et d'arrêter le container sans perdre les données persistées via la création d'un volume docker 'mongodata'.

## Création de l'image Mongo local

Se placer dans la racine du projet et exécuter la commande suivante :  
`docker build -t mongo:local .`

## Modifier le fichier docker-compose.yml pour l'adapter à votre projet

container_name: 'nom_du_container_mongo'  
MONGO_INITDB_DATABASE=nom_base_de_donnees

## Ajuster la verbosité des logs
Dans le fichier docker-compose.yml :  
ajouter des 'v' dans l'oprion de la partie command  
**exemples:**  
* command: -v  
* command: -vvvvv  

## Gestion du container
#### Lancer le container
Se placer dans la racine du projet et exécuter la commande suivante :  
`docker-compose up -d`

#### Arrêter le container
Se placer dans la racine du projet et exécuter la commande suivante :  
`docker-compose down`

