#==========================================
# Note : The "alpine" version doesn't work with Mongo mainly
# because we use MockGoose for testing.
#==========================================
FROM mongo:4.0

RUN apt-get update && \
    apt-get -y install logrotate && \
    apt-get clean all && \
    apt-get -y autoremove --purge && \
    rm -rf /var/lib/apt/lists/*